use these commands in order:

docker-compose run urlshortener python manage.py makemigrations

docker-compose run urlshortener python manage.py migrate

docker-compose up
