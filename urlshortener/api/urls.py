from django.urls import path
from .views import UrlList

app_name = 'api'

urlpatterns = [
    path('', UrlList.as_view(), name='url_list'),
]