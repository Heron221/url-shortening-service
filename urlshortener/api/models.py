from django.db import models


class URLManager(models.Manager):
    def get_queryset(self):
        return super(URLManager, self).get_queryset().filter(is_expired=False)


class URL(models.Model):
    original_url = models.URLField()
    shortened_url = models.URLField(null=True, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    expiration_time = models.DateTimeField(blank=True, null=True)  # the time that the url will be expired at
    is_expired = models.BooleanField(default=False)
    redirection_counter = models.IntegerField(default=0)
    objects = URLManager()
