from rest_framework import serializers
from .models import URL
from random import choices
from string import ascii_letters, digits


class URLSerializer(serializers.ModelSerializer):
    class Meta:
        model = URL
        fields = ['id', 'original_url', 'shortened_url', 'created_at', 'expiration_time', 'is_expired', 'redirection_counter']

    def create(self, validated_data):
        url = URL.objects.create(**validated_data)
        if 'shortened_url' not in validated_data:
            while True:
                s = ascii_letters + digits
                n = 6
                random_endpoint = ''.join(choices(s, k=n))
                short_link = 'http://127.0.0.1:8000/' + random_endpoint

                if not URL.objects.filter(shortened_url=short_link).exists():
                    break

            url.shortened_url = short_link
        url.save()
        return url
