from rest_framework import generics, status
from rest_framework.views import APIView
from .models import URL
from .serializers import URLSerializer
from django.shortcuts import redirect
import datetime
from django.utils import timezone
from rest_framework.response import Response


class UrlList(generics.ListCreateAPIView):
    queryset = URL.objects.all()
    serializer_class = URLSerializer

    def perform_create(self, serializer):
        if 'timespan' in self.request.data:
            timespan = self.request.data['timespan']
        else:
            timespan = 3  # in minutes
        serializer.save(expiration_time=timezone.now() + datetime.timedelta(minutes=timespan))


class Redirector(APIView):
    def get(self, request, *args, **kwargs):
        shortened_url = 'http://127.0.0.1:8000/' + self.kwargs['shortened_url']
        url = URL.objects.filter(shortened_url=shortened_url).first()
        if not url:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        redirect_link = url.original_url
        url.redirection_counter += 1
        url.save()
        return redirect(redirect_link)
