from django.utils import timezone
from .models import URL
from celery import Celery

app = Celery('urlshortener')


@app.task
def delete_old_urls():
    urls = URL.objects.all()
    for url in urls:
        if url.expiration_time < timezone.now():
            url.is_expired = True
            url.save()

    return f"deleted urls at {timezone.now()}"
