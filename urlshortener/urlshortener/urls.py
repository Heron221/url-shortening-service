from django.contrib import admin
from django.urls import path, include
from api.views import Redirector

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include("api.urls", namespace='api')),
    path('<str:shortened_url>', Redirector.as_view(), name='redirector'),
]
