FROM python:3
ENV PYTHONBUFFERED 1

RUN mkdir /urlshortener
WORKDIR /urlshortener

COPY ./urlshortener /urlshortener

COPY requirements.txt /urlshortener
RUN pip install -r requirements.txt
